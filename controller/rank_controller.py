from lib.hands import Hand,InvalidHand
from lib.ranker import Ranker

rank = {
    'royal_flush':1,
    'straight_flush':2,
    'four_of_a_kind':3,
    'full_house':4,
    'flush':5,
    'straight':6,
    'three_of_a_kind':7,
    'two_pair':8,
    'pair':9,
    'high_card':10
}

error = {
    'error': 'invalid poker hand'
}

class Rank_Controller(object):
    """
    Rank Controller class that has logic to rank the hand it receives.
    """
    def rank_hand(self, card_hand):
        """
        Given a card_hand, rank it using the rank dictionary
        :param card_hand: card hand received from post request
        returns rank and type of hand received
        """
        try:
            hand = Hand(card_hand['hand'])
        except InvalidHand:
            return {
                'error':'Invalid Hand'
            }

        ranker = Ranker(hand)
        if ranker.is_royal_flush():
            return {
                'rank':rank['royal_flush'],
                'type':'royal_flush'
            }
        elif ranker.is_straight_flush():
            return {
                'rank':rank['straight_flush'],
                'type':'straight_flush'
            }
        elif ranker.is_four_of_a_kind():
            return {
                'rank':rank['four_of_a_kind'],
                'type':'four_of_a_kind'
            }
        elif ranker.is_full_house():
            return {
                'rank':rank['full_house'],
                'type':'full_house'
            }
        elif ranker.is_flush():
            return {
                'rank':rank['flush'],
                'type':'flush'
            }
        elif ranker.is_straight():
            return {
                'rank':rank['straight'],
                'type':'straight'
            }
        elif ranker.is_three_of_a_kind():
            return {
                'rank':rank['three_of_a_kind'],
                'type':'three_of_a_kind'
            }
        elif ranker.is_two_pair():
            return {
                'rank':rank['two_pair'],
                'type':'two_pair'
            }
        elif ranker.is_pair():
            return {
                'rank':rank['pair'],
                'type':'pair'
            }
        else:
            return {
                'rank':rank['high_card'],
                'type':'high_card'
            }