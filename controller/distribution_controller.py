class Distribution(object):
    """
    This class creates a distribution of a given poker hand
    """
    def __init__(self):
        self.suit_hist = {}
        self.value_hist = {}

    def get_suit_distribution(self, hand):
        """
        This method creates a suit distribution for a given hand
        :param hand: hand received in request
        return suit histogram
        """
        for _hand in hand['hand']:
            if _hand['suit'] in self.suit_hist:
                self.suit_hist[_hand['suit']] += 1
            else:
                self.suit_hist[_hand['suit']] = 1
        return self.suit_hist

    def get_value_distribution(self, hand):
        """
        This method creates a value distribution for a given hand
        :param hand: hand received in request
        return value histogram
        """
        for _hand in hand['hand']:
            if _hand['suit'] in self.value_hist:
                self.value_hist[_hand['value']] += 1
            else:
                self.value_hist[_hand['value']] = 1
        return self.value_hist
