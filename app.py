from flask import jsonify,Flask,request
from controller import rank_controller,distribution_controller


app = Flask(__name__)

@app.route('/hand', methods=['POST'])
def get_rank():
    result = rank_controller.Rank_Controller().rank_hand(request.json)
    return jsonify(result)

@app.route('/stats', methods=['POST'])
def get_stats():
    suit_dist = distribution_controller.Distribution().get_suit_distribution(request.json)
    value_dist = distribution_controller.Distribution().get_value_distribution(request.json)
    return jsonify(
        {'distribution':
             {
                 'suit':suit_dist,
                 'value':value_dist
             }
        }
    )

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5455, debug=True)



