# To start the web server type python app.py #

```
#!shell
python app.py 
 * Running on http://0.0.0.0:5455/
 * Restarting with reloader
```

# Example Request - must be in json format with application/json header set #

```
#!json
{"hand":
    [
      {
        "value":"5",
        "suit": "Heart"
      },
      {
        "value":"Jack",
        "suit":"Heart"
      },
      {
        "value":"King",
        "suit": "Spade"
      },
      {
        "value":"Queen",
        "suit":"Heart"
      },
      {
        "value":"Ace",
        "suit":"Heart"
      }
    ]
}


```



# Example Response #

```
#!json
{
    "rank": 10,
    "type": "high_card"
}

```