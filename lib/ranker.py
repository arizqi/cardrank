class Ranker(object):
    """
    This class contains method that classify the Hand object into ranks
    """
    def __init__(self, hand):
        """
        :param hand: Hand object created from the hand class
        """
        self.hand = hand

    def _check_set(self, vals, *t):
        """
        Given a list, this method checks for a collection in the set e.g. three of a kind, four of a kind etc.
        :param vals: list of collections
        :param *t: validate collection
        returns True if collection is valid else returns False
        """
        for i, j in zip(t, vals):
            if i > j:
                return False
        return True

    def is_royal_flush(self):
        """
        This method checks if Hand is a royal flush
        :param None:
        """
        result = True if self.hand.same_suit and self.hand.ten_to_ace else False
        return result

    def is_straight_flush(self):
        """
        This method checks if Hand is a straight flush
        :param None:
        """
        result = True if self.hand.consecutive and self.hand.same_suit else False
        return result

    def is_four_of_a_kind(self):
        """
        This method checks if Hand is a four of a kind
        :param None:
        """
        result = self._check_set(sorted(self.hand.card_histo.values(), reverse=True), 4)
        return result

    def is_full_house(self):
        """
        This method checks if Hand is a full house
        :param None:
        """
        result = self._check_set(sorted(self.hand.card_histo.values(), reverse=True), 3, 2)
        return result

    def is_flush(self):
        """
        This method checks if Hand is a flush
        :param None:
        """
        result = self.hand.same_suit and not self.hand.consecutive
        return result

    def is_straight(self):
        """
        This method checks if Hand is a straight
        :param None:
        """
        result = self.hand.consecutive and not self.hand.same_suit
        return result

    def is_three_of_a_kind(self):
        """
        This method checks if Hand is three of a kind
        :param None:
        """
        result = self._check_set(sorted(self.hand.card_histo.values(), reverse=True), 3)
        return result

    def is_two_pair(self):
        """
        This method checks if Hand is two pair
        :param None:
        """
        result = self._check_set(sorted(self.hand.card_histo.values(), reverse=True), 2, 2)
        return result

    def is_pair(self):
        """
        This method checks if Hand is a pair
        :param None:
        """
        result = self._check_set(sorted(self.hand.card_histo.values(), reverse=True), 2)
        return result

    def is_high_card(self):
        """
        This method returns the high card of a Hand
        :param None:
        """
        result = max(self.hand.value_list)
        return result
