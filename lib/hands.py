from collections import Counter

class InvalidHand(Exception):
    """
    Invalid poker hand exception class
    """
    pass

deck = {
    '1': '1',
    '2':'2',
    '3':'3',
    '4':'4',
    '5':'5',
    '6':'6',
    '7':'7',
    '8':'8',
    '9':'9',
    '10':'10',
    'Jack':'11',
    'Queen':'12',
    'King':'13',
    'Ace':'14'
}

class Hand(object):
    """
    Class that constructs the hand object
    """
    def __init__(self, hand):
        """
        The constructor is doing some heavy lifting which needs to change
        :param hand: hand received in request
        """
        self.hand = hand
        self.suit_list = []
        self.value_list = []
        self.same_suit = None
        self.consecutive = None
        self.card_histo = None
        self.ten_to_ace = None
        self._set_suit_list()
        self._set_value_list()
        self._is_same_suit()
        self._is_consecutive()
        self._build_card_histo()
        self._validate_hand()
        self._is_ten_to_ace()

    def _set_suit_list(self):
        """
        This method creates a list of suits from the hand received
        :param None:
        """
        self.suit_list = map(lambda x: x['suit'], self.hand)

    def _set_value_list(self):
        """
        This method created a list of values from the hand received
        :param None:
        """
        self.value_list = map(lambda x: int(deck[x['value']]), self.hand)

    def _is_same_suit(self):
        """
        This method checks to see all cards have the same suit
        :param None:
        """
        if len(list(set(self.suit_list))) == 1:
            self.same_suit = True
        else:
            self.same_suit = False

    def _is_consecutive(self):
        """
        This method checks to see if the hand consists of consecutive cards
        :param None:
        """
        r = range(min(self.value_list), max(self.value_list)+1)
        if sorted(self.value_list) == r:
            self.consecutive = True
        else:
            self.consecutive = False

    def _build_card_histo(self):
        """
        This method builds a histogram of card values
        :param None:
        """
        self.card_histo = Counter(self.value_list)

    def _validate_hand(self):
        """
        This method does some basic poker hand validation
        :param None:
        """
        if len(self.value_list) != 5:
            raise InvalidHand("Invalid Poker Hand")
        elif len(list(set(self.value_list))) == 1:
            raise InvalidHand("Invalid Poker Hand")

    def _is_ten_to_ace(self):
        """
        This method checks to see if the hand is ten to ace
        :param None:
        """
        if not set(self.value_list).difference(set([10, 11, 12, 13, 14])):
            self.ten_to_ace = True
        else:
            self.ten_to_ace = False
