import unittest
from lib.ranker import Ranker
from lib.hands import Hand
 
class RankerTest(unittest.TestCase):
    """
    Ranker test cases
    """
    def setUp(self):
        self.rf = h = {"hand":
            [
              {
                "value":"10",
                "suit": "Heart"
              },
              {
                "value":"Jack",
                "suit":"Heart"
              },
              {
                "value":"King",
                "suit": "Heart"
              },
              {
                "value":"Queen",
                "suit":"Heart"
              },
              {
                "value":"Ace",
                "suit":"Heart"
              }
            ]
        }
        self.sf = {"hand":
            [
              {
                "value":"5",
                "suit": "Heart"
              },
              {
                "value":"6",
                "suit":"Heart"
              },
              {
                "value":"7",
                "suit": "Heart"
              },
              {
                "value":"8",
                "suit":"Heart"
              },
              {
                "value":"9",
                "suit":"Heart"
              }
            ]
        }
        self.four_of_a_kind = {"hand":
            [
              {
                "value":"5",
                "suit": "Heart"
              },
              {
                "value":"5",
                "suit":"Club"
              },
              {
                "value":"5",
                "suit": "Ace"
              },
              {
                "value":"5",
                "suit":"Spade"
              },
              {
                "value":"7",
                "suit":"Heart"
              }
            ]
        }
        self.full_house =  {"hand":
            [
              {
                "value":"5",
                "suit": "Heart"
              },
              {
                "value":"5",
                "suit":"Club"
              },
              {
                "value":"5",
                "suit": "Spade"
              },
              {
                "value":"6",
                "suit":"Heart"
              },
              {
                "value":"6",
                "suit":"Spade"
              }
            ]
        }


    def test_royal_flush(self):
        self.assertTrue(Ranker(Hand(self.rf['hand'])).is_royal_flush())

    def test_straight_flush(self):
        self.assertTrue(Ranker(Hand(self.sf['hand'])).is_straight_flush())

    def test_four_of_a_kind(self):
        self.assertTrue(Ranker(Hand(self.four_of_a_kind['hand'])).is_four_of_a_kind())

    def test_full_house(self):
        self.assertTrue(Ranker(Hand(self.full_house['hand'])).is_full_house())
     
